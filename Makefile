install:
	docker exec vagrant_app_1 npm install

run-dev:
	docker exec vagrant_app_1 npm run dev

run:
	docker exec vagrant_app_1 npm start

unit-tests:
	docker exec vagrant_app_1 npm test