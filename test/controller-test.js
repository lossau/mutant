var fs = require('fs')
const { expect } = require('chai')
const {
    getUsersWebsites,
    getUsersData,
    filterByAddress } = require('../src/controller')


const readJson = (path, cb) => {
    fs.readFile(require.resolve(path), (err, data) => {
        if (err)
            cb(err)
        else
            cb(null, JSON.parse(data))
    })
}

describe('getUsersWebsites()', function () {
    it('should return each user\'s id and website', function () {

        const expected = [
            {
                id: 1,
                website: "hildegard.org"
            },
            {
                id: 2,
                website: "anastasia.net"
            },
            {
                id: 3,
                website: "ramiro.info"
            }
        ]

        readJson('./users-mock.json', (err, payload) => {
            const actual = getUsersWebsites(payload)
            expect(actual).to.deep.have.same.members(expected)
        })

    })
})

describe('getUsersData()', function () {
    it('should return each user\'s name, email and company name, ordered by name', function () {

        const expected = [
            {
                id: 3,
                name: 'Clementine Bauch',
                email: 'Nathan@yesenia.net',
                company: 'Romaguera-Jacobson'
            },
            {
                id: 2,
                name: 'Ervin Howell',
                email: 'Shanna@melissa.tv',
                company: 'Deckow-Crist'
            },
            {
                id: 1,
                name: 'Leanne Graham',
                email: 'Sincere@april.biz',
                company: 'Romaguera-Crona'
            },
        ]

        readJson('./users-mock.json', (err, payload) => {
            const actual = getUsersData(payload)
            expect(actual).to.deep.equal(expected)
        })

    })
})

describe('filterByAddress()', () => {
    it('should return users with an address containing the word "suite"', () => {

        const expected = [
            {
                "id": 2,
                "name": "Ervin Howell",
                "username": "Antonette",
                "email": "Shanna@melissa.tv",
                "address": {
                    "street": "Victor Plains",
                    "suite": "Suite 879",
                    "city": "Wisokyburgh",
                    "zipcode": "90566-7771",
                    "geo": {
                        "lat": "-43.9509",
                        "lng": "-34.4618"
                    }
                },
                "phone": "010-692-6593 x09125",
                "website": "anastasia.net",
                "company": {
                    "name": "Deckow-Crist",
                    "catchPhrase": "Proactive didactic contingency",
                    "bs": "synergize scalable supply-chains"
                }
            },
            {
                "id": 3,
                "name": "Clementine Bauch",
                "username": "Samantha",
                "email": "Nathan@yesenia.net",
                "address": {
                    "street": "Douglas Extension",
                    "suite": "Suite 847",
                    "city": "McKenziehaven",
                    "zipcode": "59590-4157",
                    "geo": {
                        "lat": "-68.6102",
                        "lng": "-47.0653"
                    }
                },
                "phone": "1-463-123-4447",
                "website": "ramiro.info",
                "company": {
                    "name": "Romaguera-Jacobson",
                    "catchPhrase": "Face to face bifurcated interface",
                    "bs": "e-enable strategic applications"
                }
            }
        ]

        readJson('./users-mock.json', (err, payload) => {
            const actual = filterByAddress(payload, 'suite')
            expect(actual).to.deep.equal(expected)
        })

    })
})