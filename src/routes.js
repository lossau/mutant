var express = require('express')
const service = require('./service')
var router = express.Router()


router.use((req, res, next) => {
    console.log(`--- Request recebida em: ${req.url}`)
    next();
});

router.get('/users-websites', async (req, res) => {
    const response = await service.getUsersWebsites()
    res.send(response)
})

router.get('/users-data', async (req, res) => {
    const response = await service.getUsersData()
    res.send(response)
})

router.get('/users-with-suite', async (req, res) => {
    const response = await service.filterByAddress('suite')
    res.send(response)
})


module.exports = router