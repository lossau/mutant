const config = require('../config')
const elasticsearch = require('../lib/elasticsearch')
const controller = require('./controller')


const getData = async (url) => {
    return controller.getData(url)
}

const getUsersWebsites = async () => {
    const users = await getData(config.dataUrl)
    const response = controller.getUsersWebsites(users)
    elasticsearch.addDocument(
        'users-websites',
        'request',
        { data: response }
    )
    return response
}

const getUsersData = async () => {
    const users = await getData(config.dataUrl)
    const response = controller.getUsersData(users)
    elasticsearch.addDocument(
        'users-data',
        'request',
        { data: response }
    )
    return response
}

const filterByAddress = async (keyword) => {
    const users = await getData(config.dataUrl)
    const response = controller.filterByAddress(users, keyword)
    elasticsearch.addDocument(
        'users-with-suite',
        'request',
        { data: response }
    )
    return response
}


module.exports = {
    getData,
    getUsersWebsites,
    getUsersData,
    filterByAddress
}