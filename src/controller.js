const axios = require("axios")


const getData = async (url) => {
    try {
        const response = await axios.get(url)
        return response.data
    } catch (error) {
        return error
    }
}

const getUsersWebsites = (users) => {
    return users.map(u => {
        return {
            id: u.id,
            website: u.website
        }
    })
}

const getUsersData = (users) => {
    return users
        // remove títulos de nomes como Mr., Mrs. para ordenação
        .map(u => {
            u.name_sort = u.name.replace(/Mr\.|Mrs\./g, '').trim()
            return u
        })
        // ordena alfabeticamente por nome
        .sort((a, b) => a.name_sort.localeCompare(b.name_sort))
        // retorna os campos necessários
        .map(u => {
            return {
                id: u.id,
                name: u.name,
                email: u.email,
                company: u.company.name
            }
        })
}

const filterByAddress = (users, keyword) => {
    return users.filter(u => {
        return u.address.suite.toLowerCase().includes(keyword)
    })
}


module.exports = {
    getData,
    getUsersWebsites,
    getUsersData,
    filterByAddress
}