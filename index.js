const express = require('express')
const app = express()
const config = require('./config')

const port = config.serverPort

app.use(require('./src/routes'))

app.listen(port, () => {
    console.log(`- Rodando mutant em: http://localhost:${port}`)
})