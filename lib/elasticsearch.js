var elasticsearch = require('elasticsearch');
const config = require('../config')

var client = new elasticsearch.Client({
	host: config.elasticsearchHost
});


module.exports = {

	addDocument: (indexName, docType, payload) => {
		now = new Date()
		payload.timestamp = now.toISOString()
		client.index({
			index: indexName,
			type: docType,
			body: payload
		}, function (err, resp, status) {
			console.log(`--- Registrado no Elasticsearch no índice: '${indexName}' com id: '${resp._id}'`);
		});
	}

};