# Aplicativo para processo seletivo da Mutant

> Sua tarefa é fazer um aplicativo que carregue a saida da URL https://jsonplaceholder.typicode.com/users, que retorna uma lista de usuário em JSON.
>Faça um programa que carregue a saída dessa URL e mostre os seguintes dados:
>1. Os websites de todos os usuários
>2. O Nome, email e a empresa em que trabalha (em ordem alfabética).
>3. Mostrar todos os usuários que no endereço contem a palavra ```suite```
>4. Salvar logs de todas interações no elasticsearch
>5. EXTRA: Criar test unitário para validar os itens a cima.


## Requisitos:
- VirtualBox
- Vagrant


## Instruções

#### Clone o repositório
    git clone git@bitbucket.org:lossau/mutant.git
#### No terminal, vá para o diretório clonado
    cd mutant/
#### Instale o plugin de Docker Composer do Vagrant
    vagrant plugin install vagrant-docker-compose
#### Suba o Vagrant
##### instala dependências e inicia toda a aplicação, pode demorar alguns minutos

    vagrant up

## Como Usar
A aplicação expõe uma API com 3 endpoints no endereço: http://localhost:8080

- http://localhost:8080/users-websites
- http://localhost:8080/users-data
- http://localhost:8080/users-with-suite

## Testes Unitários
Para rodar os testes, é necessário acessar a VM do Vagrant por SSH

#### No terminal, vá para a pasta da aplicação
    cd mutant/
#### Acesso o SSH exposto pelo Vagrant
    vagrant ssh
#### Vá para a pasta da aplicação, dentro do Vagrant
    cd /vagrant
#### Rode os testes
    make unit-tests

## Elasticsearch
As requisições ao servidor são registradas no elasticsearch, acessível no endereço: http://localhost:9200

#### Buscas nos índices
Requisições HTTP para buscas nos índices da aplicação

- http://localhost:9200/users-websites/_search
- http://localhost:9200/users-data/_search
- http://localhost:9200/users-with-suite/_search
